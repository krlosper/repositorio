const { request } = require("express");
const fs = require('fs');
const express= require('express');
const app = express();
const bodyParser=require('body-parser');
const multipart =require('connect-multiparty');
const cors=require('cors');
const puerto =3000;
app.use(cors());

const multiPartMiddleware =multipart({
    uploadDir:'./subidas'
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
extended:true   
}));
app.post('/api/subir/:id',multiPartMiddleware,(req,res)=>{   
      fs.rename(__dirname+"\\"+req.files.uploads[0].path, __dirname+"/subidas/"+req.params.id+"_"+req.files.uploads[0].name, function (err) {
        if (err) throw err;
        console.log('Nombre Editado');
      });
      console.log()
    res.json({
        'message':'guardado correctamente',
        'DirDocu':req.files.uploads[0].path
    });
    });
app.get('/Archivos',(req,res)=>{
    fs.readdir(__dirname+"/subidas/",  
        { withFileTypes: true }, 
        (err, files) => { 
        if (err) 
          console.log(err); 
        else { 
            res.json(files);
        } 

      }) 
});
app.get('/descargas/:id', function(req, res) {
   // res.send('valors: ' + req.params.id);    
    var file = __dirname +'/subidas/'+req.params.id; 
   res.download(file); 
  });
app.listen(puerto,()=>console.log('port 3000'));