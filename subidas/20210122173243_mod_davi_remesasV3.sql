DECLARE 
SRV_MESSAGE         VARCHAR2(200);
IN_FLD_OPR_COR_EVE  NUMBER;
IN_FLD_OPR_FEC_EVE  VARCHAR2(200);
IN_FLD_OPR_COD_EVE  NUMBER;
IN_FLD_OPR_SUC_EVE  NUMBER;
IN_FLD_OPR_USR_EVE  VARCHAR2(200);
IN_FLD_OPR_TXT_EVE  VARCHAR2(200);
OUT_RESULT_MSG      VARCHAR2(200);
IN_XWSS_RESULT_SW   NUMBER;
IN_TRAN_COUNT       NUMBER;
xFLD_OPR_IDE_ORD    VARCHAR2(14);     
xMsg                Varchar2(250);

BEGIN 
SRV_MESSAGE         := NULL;
IN_FLD_OPR_COR_EVE  := '0';
IN_FLD_OPR_FEC_EVE  := SYSDATE;
IN_FLD_OPR_COD_EVE  := 998;--atdrevento
IN_FLD_OPR_SUC_EVE  := 7000;--acmxsucsal
IN_FLD_OPR_USR_EVE  := 'MCCALLEJ';
IN_FLD_OPR_TXT_EVE  := 'Proc Diario ModifEst a DEV RM259646';
OUT_RESULT_MSG      := NULL;
IN_XWSS_RESULT_SW   := NULL;
IN_TRAN_COUNT       := NULL;

    Delete  Tdummy
    where   Num = 20200605;
    Commit;

    Grabartdummy(20200605,'Inicia Proceso ['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');

    For Cur in (
        select distinct(tabla.FLD_OPR_NUM_ISN) FLD_OPR_NUM_ISN, tabla.FLD_OPR_REF_EXT FLD_OPR_REF_EXT
        from (
            SELECT  t.FLD_OPR_NUM_ISN FLD_OPR_NUM_ISN, t.FLD_OPR_REF_EXT FLD_OPR_REF_EXT, '1' Pos
            FROM    TDR T, OPREVETR O, OPRGLB G
            WHERE   T.FLD_M_32A_FVAL between TRUNC(SYSDATE)-13 and to_date('2020/10/31','YYYY/MM/DD')
            AND     T.FLD_OPR_COD_EST IN ('APP','OBJ')
            and     ismoneylinea(t.FLD_OPR_MSG_TIPO) = 0
            AND     T.FLD_OPR_NUM_ISN  = G.FLD_OPR_NUM_ISN
            AND     G.FLD_OPR_IND_FPAG = '1' --Abono a Cuenta
            AND     T.FLD_OPR_NUM_ISN  = O.FLD_OPR_NUM_ISN  --Lavado
            AND     O.FLD_OPR_COD_EVE IN (SELECT COD_EVE FROM ATDREVENTO where eve_rel = 350)
            union
            SELECT  t.FLD_OPR_NUM_ISN FLD_OPR_NUM_ISN, t.FLD_OPR_REF_EXT FLD_OPR_REF_EXT, '2' Pos
            FROM    TDR T, OPREVETR O, OPRGLB G
            WHERE   T.FLD_M_32A_FVAL between TRUNC(SYSDATE)-13 and to_date('2020/10/31','YYYY/MM/DD')
            AND     T.FLD_OPR_COD_EST IN ('APP','OBJ')
            and     ismoneylinea(t.FLD_OPR_MSG_TIPO) = 0
            AND     T.FLD_OPR_NUM_ISN  = G.FLD_OPR_NUM_ISN
            AND     G.FLD_OPR_IND_FPAG = '1' --Abono a Cuenta
            AND     T.FLD_OPR_NUM_ISN  = O.FLD_OPR_NUM_ISN
            AND     O.FLD_OPR_COD_EVE IN (114,115,116)--Fallo En Comunicacion Sun-One
            AND     (O.FLD_OPR_TXT_EVE LIKE '%TIPO DE CTA NO VALIDO%' OR O.FLD_OPR_TXT_EVE LIKE '%TRANSACCION NO PERMITIDA%')
            union
            SELECT  t.FLD_OPR_NUM_ISN FLD_OPR_NUM_ISN, t.FLD_OPR_REF_EXT FLD_OPR_REF_EXT, '3' Pos
            FROM    TDR T, OPREVETR O, OPRGLB G
            WHERE   T.FLD_M_32A_FVAL between TRUNC(SYSDATE)-13 and to_date('2020/10/31','YYYY/MM/DD')
            AND     T.FLD_OPR_COD_EST IN ('APP','OBJ')
            and     ismoneylinea(t.FLD_OPR_MSG_TIPO) = 0
            AND     T.FLD_OPR_NUM_ISN  = G.FLD_OPR_NUM_ISN
            AND     G.FLD_OPR_IND_FPAG = '1' --Abono a Cuenta
            AND     T.FLD_OPR_NUM_ISN  = O.FLD_OPR_NUM_ISN
            AND     O.FLD_OPR_COD_EVE  = 99 --Cliente en Observacion
            AND     O.FLD_OPR_TXT_EVE LIKE '%Intento de Pago Automatico Cliente en Observacion%'
            union
            SELECT  t.FLD_OPR_NUM_ISN FLD_OPR_NUM_ISN, t.FLD_OPR_REF_EXT FLD_OPR_REF_EXT, '4' Pos
            FROM    TDR T, OPREVETR O, OPRGLB G
            WHERE   T.FLD_M_32A_FVAL between TRUNC(SYSDATE)-13 and to_date('2020/10/31','YYYY/MM/DD')
            AND     T.FLD_OPR_COD_EST = 'OBJ'
            and     ismoneylinea(t.FLD_OPR_MSG_TIPO) = 0
            AND     T.FLD_OPR_NUM_ISN  = G.FLD_OPR_NUM_ISN
            AND     G.FLD_OPR_IND_FPAG = '1' --Abono a Cuenta
            AND     T.FLD_OPR_NUM_ISN  = O.FLD_OPR_NUM_ISN
            AND     O.FLD_OPR_COD_EVE  = 109--OBJETADA - LISTA RESTRICTIVA 
            AND     (O.FLD_OPR_TXT_EVE LIKE '%733%' OR O.FLD_OPR_TXT_EVE LIKE '%120%' OR O.FLD_OPR_TXT_EVE LIKE '%311%' OR O.FLD_OPR_TXT_EVE LIKE '%130%')) tabla ) Loop
    
        Begin    
            update  tdr
            set     FLD_OPR_COD_EST     = 'DEV'
                    ,FLD_OPR_NUM_AVI    = 999
            where   FLD_OPR_NUM_ISN     = Cur.fld_opr_num_isn;
            COMMIT;
        Exception When Others Then
            xMsg := substr(trim(sqlerrm),1,250); 
            Grabartdummy(20200605,'Error Act TDR:['||TRIM(Cur.fld_opr_num_isn)||'|'||xMsg||'] Proceso:['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');
            goto  siguiente;
        End;
            
        Begin
            TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve
                    (SRV_Message,
                    Cur.fld_opr_num_isn,
                    IN_FLD_OPR_COR_EVE,
                    IN_FLD_OPR_FEC_EVE,
                    IN_FLD_OPR_COD_EVE,
                    IN_FLD_OPR_SUC_EVE, 
                    IN_FLD_OPR_USR_EVE, 
                    IN_FLD_OPR_TXT_EVE, 
                    OUT_RESULT_MSG, 
                    IN_XWSS_RESULT_SW, 
                    IN_TRAN_COUNT );                
            COMMIT;
            
            if substr(trim(SRV_MESSAGE),1,1) <> '1' then
                xMsg := substr(trim(SRV_MESSAGE),1,250); 
                Grabartdummy(20200605,'1.Error Ingreso Evento TDR:['||TRIM(Cur.fld_opr_num_isn)||'|'||xMsg||'] Proceso:['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');        
                goto  siguiente;
            end if;
        Exception When Others Then
            xMsg := substr(trim(sqlerrm),1,250); 
            Grabartdummy(20200605,'2.Error Ingreso Evento TDR:['||TRIM(Cur.fld_opr_num_isn)||'|'||xMsg||'] Proceso:['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');    
            goto  siguiente;
        End;
        
        Grabartdummy(20200605,'Referencia Procesada OK:['||TRIM(Cur.fld_opr_num_isn)||']['||TRIM(Cur.fld_opr_ref_ext)||'] Proceso:['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');
<<siguiente>>
    null;
    End Loop;            
    Grabartdummy(20200605,'Finaliza Proceso ['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');
END; 
/
set line 2000
set pagesize 2000
set serveroutput on size 999999
set linesize 5000
set feedback on
set trimspool on
set heading on
set serveroutput on
SELECT  TRIM(DESCRIP)
FROM    TDUMMY
WHERE   NUM = 20200605;
exit
  

